from vehiculo import Vehiculo
class Motocicleta(Vehiculo):
    def __init__(self, placa="", marca="", linea="", modelo=0, cilindraje=0, color="",
                 num_chasis="", num_motor="", potencia=0, pasajeros=2, ciud_matri="", transito_de=""):
       
        super().__init__(placa, marca, linea, modelo, cilindraje, color)
        
       
        self._num_chasis = num_chasis
        self._num_motor = num_motor
        self._potencia = potencia
        self._pasajeros = pasajeros
        self._ciud_matri = ciud_matri
        self._transito_de = transito_de

    @property
    def num_chasis(self):
        return self._num_chasis

    @num_chasis.setter
    def num_chasis(self, valor):
        self._num_chasis = valor

    @property
    def num_motor(self):
        return self._num_motor

    @num_motor.setter
    def num_motor(self, valor):
        self._num_motor = valor

    @property
    def potencia(self):
        return self._potencia

    @potencia.setter
    def potencia(self, valor):
        self._potencia = valor

    @property
    def pasajeros(self):
        return self._pasajeros

    @pasajeros.setter
    def pasajeros(self, valor):
        self._pasajeros = valor

    @property
    def ciud_matri(self):
        return self._ciud_matri

    @ciud_matri.setter
    def ciud_matri(self, valor):
        self._ciud_matri = valor

    @property
    def transito_de(self):
        return self._transito_de

    @transito_de.setter
    def transito_de(self, valor):
        self._transito_de = valor
