from conexion import Conexion
import pandas as pd
import attr

@attr.s
class Vehiculo(Conexion):
    
    def __init__(self, placa="", marca="", linea="", modelo=0, cilindraje=0, color=""):
        self._placa = placa
        self._marca = marca
        self._linea = linea
        self._modelo = modelo
        self._cilindraje = cilindraje
        self._color = color
        
        super().__init__()
        self.create_vehiculo()
        self.__ruta = ".entregable_2/src/static/xlsx/clase.xlsx"
        self.__df = pd.read_excel(self.__ruta,sheet_name="vehiculo")
        self.insert_vehiculo()
 
       
        
    @property
    def placa(self):
        return self._placa

    @placa.setter
    def placa(self, valor):
        self._placa = valor

    @property
    def marca(self):
        return self._marca

    @marca.setter
    def marca(self, valor):
        self._marca = valor

    @property
    def linea(self):
        return self._linea

    @linea.setter
    def linea(self, valor):
        self._linea = valor

    @property
    def modelo(self):
        return self._modelo

    @modelo.setter
    def modelo(self, valor):
        self._modelo = valor

    @property
    def cilindraje(self):
        return self._cilindraje

    @cilindraje.setter
    def cilindraje(self, valor):
        self._cilindraje = valor

    @property
    def color(self):
        return self._color

    @color.setter
    def color(self, valor):
        self._color = valor
        


    def create_vehiculo(self):
        atributos = vars(self)
        if self.crear_tabla(nom_tabla="vehiculo",datos_tbl="datos_vehiculo"):
            print("Tabla vehiculo Creada!!!")
        
        return atributos
    
    def insert_vehiculo(self):
        
        for index, row in self.__df.iterrows():
            datos = '{},"{}","{}","{}",{},{},"{}",'.format(row["placa"],row["marca"],row["linea"],row["modelo"],row["cilindraje"],row["color"])
            print (datos)
            self.insertar_datos(nom_tabla="vehiculo",nom_columns="carga_vehiculo",datos_carga=datos)
        return True
    
    
    
    def select_vehiculo(self,placa=[]):
        return Vehiculo
        
    
        
    def __str__(self) :
        return "Vehiculo : placa {}  marca {}".format(self.__placa,self.__marca)