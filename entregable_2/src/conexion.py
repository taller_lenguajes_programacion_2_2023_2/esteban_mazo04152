import sqlite3
import json
import re
import pandas as pd


class Conexion:
    def __init__(self) :
        self.__user=""
        self.__password=""
        self.__puerto=0
        self.__url=""
        __nom_db="datos.sqlite"

        self.__querys = self.obtener_json()
        self.__conx = sqlite3.connect(__nom_db)
        #self.__cursor = conx.cursor()
    
    def obtener_json(self):
      
        ruta = ".entregable_2/src/static/sql/querys.json"
        querys={}
        with open(ruta, 'r') as file:
            querys = json.load(file) 
        return querys
    
    def crear_tabla(self, nom_tabla="",datos_tbl = ""):
        if nom_tabla != "" :
            datos=self.__querys[datos_tbl]
            query =  self.__querys["create_tb"].format(nom_tabla,datos)
            self.__conx.execute(query)
            return True
        else:
            return False
        
    def insertar_datos(self, nom_tabla="",nom_columns = "", datos_carga=""):
        if nom_tabla != "" :
            columnas=self.__querys[nom_columns]
            query =  self.__querys["insert"].format(nom_tabla,columnas,datos_carga)
            self.__conx.execute(query)
            self.__conx.commit()
            return True
        else:
            return False
   
conx = Conexion()
conx.crear_tabla("Vehiculo", "datos_vehiculo")


#elect = conx.execute("select * from vehiculo ")#
#print(select) 
    
    
