class Vehiculo :
    def __init__(self,placa="", marca="", linea="",modelo="",cilindraje="",color=""
    ):
        
        self.placa = placa
        self.marca = marca
        self.linea = linea
        self.modelo = modelo
        self.cilindraje = cilindraje
        self.color = color


    def __str__(self) :
        """funcion para visualizar objeto vehiculo"""
        return "{} object {}".format(self.placa,self.marca)

veh = Vehiculo(placa = "hoal",marca="hola")
veh.__str__()
