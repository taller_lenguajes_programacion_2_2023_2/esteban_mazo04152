from django.db import models

# Create your models here.
class Persona(models.Model):
    cedula = models.IntegerField (unique=True)
    nombre  = models.CharField(max_length=20)
    apellido1 = models.CharField(max_length=20)
    apellido2 = models.CharField(max_length=20)
    telefono = models.IntegerField ()

    def __str__(self):
        return f"{self.cedula} {self.nombre} {self.apellido1} {self.apellido2} {self.telefono}"


class Usuario(Persona):
    correo = models.CharField(max_length=30,unique=True)
    password = models.CharField(max_length=20)
    conf_pass = models.CharField(max_length=20)
    rol_usuario= models.CharField(max_length=20)
    fecha_cre = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f"Usuario: {self.correo} {self.password} {self.conf_pass} {self.rol_usuario} - {super().__str__()}"