from django.contrib import admin

from django import forms
from store.models import Producto




# Register your models here.
@admin.register(Producto)
class ProductoAdmin(admin.ModelAdmin):
    list_display = ('nombre', 'precio', 'descripcion', 'imagen')
    list_filter = ('nombre', 'precio')
    search_fields = ('nombre', 'descripcion')


    fieldsets = (
        (None, {
            'fields': ('nombre', 'precio', 'descripcion', 'imagen')
        }),
    )
