from django.contrib.auth import login as auth_login
from django.shortcuts import render, redirect, reverse

from projects.models import Usuario
from .forms import RegistroForm, LoginForm


def registro(request):
    data = {'passwordError': ''}
    if request.method == 'POST':
        print("FormData", request.POST)
        form = RegistroForm(data=request.POST)
        data["registroForm"] = form
        usuariodb = Usuario.objects.filter(correo=form.data["correo"])
        if len(usuariodb) > 0:
                data["correoError"] = "Este correo ya esta registrado"
                return render(request, 'registro.html', data)
        if form.is_valid():
            print("Contrasenas ", form.data["password"], form.data["conf_pass"])
            if form.data["password"] == form.data["conf_pass"]:
                form.save()
                return redirect('/login/')
            else:
                data["passwordError"] = "Las contraseñas no coinciden"
                return render(request, 'registro.html', data)
        else:
            print("Formulario no valido", form.errors)
            return render(request, 'registro.html', data)
    elif request.method == 'GET':
        form = RegistroForm(data=request.GET)
        data["registroForm"] = form
        return render(request, 'registro.html', data)


# def obtenerUsuario(correo):
#     try:
#         usuario = Usuario.objects.filter(correo=correo, password=password)
#         print("usuario", usuario)
#         if len(usuario) > 0:
#             return usuario.first()
#         else:
#             return None
#     except Exception as e:
#         print("Error", e)
#         return None


def authenticate(request, correo, password):
    try:
        usuariodb = Usuario.objects.filter(correo=correo, password=password)
        print("usuariodb", usuariodb)
        if len(usuariodb) > 0:
            return True, usuariodb.first()
        else:
            return False, "Usuario/Contraseña No valido"
    except Exception as e:
        print("Error", e)
        return False, "Usuario/Contraseña No valido"


def login(request):
    # return HttpResponse("estoy en la pagina de login")
    loginForm = {'txt_login': "Ingrese Usuario", 'estado': ""}
    if request.user.is_authenticated:
        return redirect(reverse("store"))
    if request.method == 'POST':
        form = LoginForm(data=request.POST)
        if form.is_valid():
            correo = request.POST.get('correo')
            password = request.POST.get('password', '')
            authentication = authenticate(request, correo=correo, password=password)
            isAuthenticated = authentication[0]
            if isAuthenticated:
                loginForm["estado"] = "valido"
                auth_login(request, authentication[1])
                print("Usuario", authentication[1])
                return redirect('/store')
            else:
                loginForm["estado"] = authentication[1]
                loginForm["loginForm"] = form
                return render(request, "login.html", loginForm)
        else:
            loginForm["estado"] = "Usuario/Contraseña No valido"
            loginForm['loginForm'] = form
            return render(request, 'login.html', loginForm)
    else:
        form = LoginForm(data=request.GET)
        loginForm['loginForm'] = form
        return render(request, 'login.html', loginForm)
