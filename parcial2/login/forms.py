from django import forms
from django.core.validators import RegexValidator

from projects.models import Usuario

numeric = RegexValidator(r'^[0-9+]', 'Solo se admiten digitos en este campo.')


class RegistroForm(forms.ModelForm):
    cedula = forms.CharField(required=True, widget=forms.NumberInput(attrs={'placeholder': 'Cédula'}
                                                                     ), validators=[numeric])
    nombre = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'Nombre'}
    ), min_length=3)
    apellido1 = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'Apellido 1'}
    ), min_length=3)
    apellido2 = forms.CharField(required=True, widget=forms.TextInput(
        attrs={'placeholder': 'Apellido 2'}
    ), min_length=3)
    correo = forms.CharField(required=True, widget=forms.EmailInput(
        attrs={'placeholder': 'Correo electrónico'}
    ))
    password = forms.CharField(required=True, widget=forms.PasswordInput(
        attrs={'placeholder': 'Contraseña'}
    ))
    conf_pass = forms.CharField(required=True, widget=forms.PasswordInput(
        attrs={'placeholder': 'Confirme la contraseña'}
    ))
    telefono = forms.CharField(required=True, widget=forms.NumberInput(
        attrs={'placeholder': 'Teléfono'}
    ), validators=[numeric])
    is_superuser = forms.BooleanField(required=False, widget=forms.CheckboxInput(
    ))

    class Meta:
        model = Usuario
        fields = ["cedula", "nombre", "apellido1", "apellido2", "correo", "password", "conf_pass", "telefono",
                  "is_superuser"]



class LoginForm(forms.Form):
    correo = forms.CharField(required=True, widget=forms.EmailInput(
        attrs={'placeholder': 'Correo'}
    ))
    password = forms.CharField(required=True, widget=forms.PasswordInput(
        attrs={'placeholder': 'Contraseña'}
    ))

    class Meta:
        model = Usuario
        fields = ["correo", "password"]
