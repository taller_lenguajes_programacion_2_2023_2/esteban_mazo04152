
/* login */
function login() {
  let usuarios = localStorage.getItem("usuarios");
  if(usuarios == null){
    alert("Credenciales inválidas");
    return;
  }else {
    usuarios = JSON.parse(usuarios);
    if (usuarios.length == 0){
      alert("Credenciales inválidas");
      return;
    }

  }


    var correo = document.getElementById("correo").value;
    var contrasena = document.getElementById("contrasenaLogin").value;
    if (correo.trim()=== "" || contrasena.trim()===""){
          window.alert("Error: Hay campos vacios!!")
          return;
    } 

    usuarios = JSON.parse(localStorage.getItem("usuarios"));
    const usuario = usuarios.find(usuario => usuario.correo == correo.trim() && usuario.contrasena == contrasena.trim());
    if (usuario){
      window.location.href = 'store.html';
      localStorage.setItem('loggedUser', JSON.stringify({
        nombre: usuario.nombre,
        apellido1: usuario.apellido1,
        apellido2: usuario.apellido2,
        correo: usuario.correo,
        telefono: usuario.telefono,
        "rol_usuario": usuario.rol_usuario
      }));
    }else{
      alert("Credenciales inválidas");
    }

  }
/*Registro*/

function registro() {
  
  // console.log("Mensaje de ejecicion registro");
  var cedula = document.getElementById("cedula").value;
  var nombre = document.getElementById("nombre").value;
  var apellido1 = document.getElementById("apellido1").value;
  var apellido2 = document.getElementById("apellido2").value;
  var correo = document.getElementById("email").value;
  var contrasena = document.getElementById("contrasena").value;
  var contrasena2 = document.getElementById("contrasena2").value;
  var telefono = document.getElementById("telefono").value;
  var rol_usuario = document.getElementById("rol_usuario").value;

  if (cedula.trim()=== "" || nombre.trim()==="" || apellido1.trim()==="" || apellido2.trim()==="" || correo.trim()==="" || contrasena.trim()==="" ||apellido2.trim()===""|| rol_usuario.trim()==="" || contrasena2.trim()==="" ){
    window.alert("Error: Hay campos vacios!!")
    return;
}

  const usuario = {
    cedula,
    nombre,
    apellido1,
    apellido2,
    correo,
    contrasena,
    contrasena2,
    telefono,
    rol_usuario,
  }

  let usuarios = localStorage.getItem("usuarios");
  if (usuarios !== null){
    usuarios = JSON.parse(usuarios);
    usuarios.push(usuario);
    localStorage.setItem('usuarios', JSON.stringify(usuarios));
  }else{
    usuarios = [];
    usuarios.push(usuario);
    localStorage.setItem('usuarios', JSON.stringify(usuarios));
  }
  if (contrasena==contrasena2){
    window.alert("DATOS REGISTRADOS EXITOSAMENTE");
    window.location.href = 'login.html';
  }else {
    window.alert("contrasenias no coinciden");
  }
  

  //console.log("Correo", correo);

 /* var usuario = {
    nombre: nombre,
    correo: correo,
    contrasena: contrasena,
    telefono: telefono,
  };

  fetch("http://localhost:8080/user/registro", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(usuario),
  })
    .then((response) => response.text())
    .then((data) => {
      alert(data);
    })
    .catch((error) => {
      console.error("Error:", error);
    });*/
}
