from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Producto


# Create your views here.
@login_required
def store(request):
    productos = Producto.objects.all()
    return render(request, 'store.html', {'productos': productos})
