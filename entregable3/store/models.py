from django.db import models


# Create your models here.
class Producto(models.Model):
    id = models.AutoField(primary_key=True, unique=True)
    nombre = models.CharField(max_length=250, null=True)
    precio = models.FloatField(null=True)
    descripcion = models.CharField(max_length=250, null=True)
    imagen = models.ImageField(upload_to='productos', null=True)

    def __str__(self):
        return f"{self.nombre} {self.precio} {self.descripcion} {self.imagen}"


