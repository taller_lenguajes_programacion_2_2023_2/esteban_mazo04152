from projects.models import Usuario
from rest_framework import viewsets, permissions
from .serializers import UsuarioSerializer,PersonaSerializer

#@viewsets(['POST'])

class UsuarioViewSet(viewsets.ModelViewSet):
    queryset = Usuario.objects.all()
    permission_classes = [permissions.AllowAny]
    serializer_class = UsuarioSerializer

class PersonaViewSet(viewsets.ModelViewSet):
    queryset = Usuario.objects.all()
    permission_classes = [permissions.AllowAny]
    serializer_class = PersonaSerializer