from rest_framework import routers
from .api import UsuarioViewSet,PersonaViewSet
router = routers.DefaultRouter()


#router.register('api/persona',PersonaViewSet, 'persona')
router.register('api/user',UsuarioViewSet, 'user')
router.register('api/persona',PersonaViewSet,'persona')
urlpatterns = router.urls