var products = [
  {
    id: "producto1",
    nombre: "Hamburgesa chiken",
    precio: 25000,
    img: '<img style="width:70px;height:60px" src="img/H_chiken.jpg" alt="">',
    stock: 2,
    carrito: 0,
  },
  {
    id: "producto2",
    nombre: "Hamburgesa Chiken Especial",
    precio: 27000,
    img: '<img style="width:70px;height:60px" src="img/H_chikenespecial.jpeg" alt="">',
    stock: 2,
    carrito: 0,
  },
  {
    id: "producto3",
    nombre: "Hamburgesa Chiken Fingers",
    precio: 30000,
    img: '<img style="width:70px;height:60px" src="img/H_chikenfingers.jpg" alt="">',
    stock: 2,
    carrito: 0,
  },
  {
    id: "producto4",
    nombre: "Haburguesa Chiken Champiñon",
    precio: 31000,
    img: '<img style="width:70px;height:60px" src="img/H_chikenchamiñon.png" alt="">',
    stock: 2,
    carrito: 0,
  },
  {
    id: "producto5",
    nombre: "Hamburguesa Pig",
    precio: 25000,
    img: '<img style="width:70px;height:60px" src="img/H_pig.jpg" alt="">',
    stock: 3,
    carrito: 0,
  },
  {
    id: "producto6",
    nombre: "Hamburguesa Pig Especial",
    precio: 28000,
    img: '<img style="width:70px;height:60px" src="img/H_pigespecial.jpg" alt="">',
    stock: 2,
    carrito: 0,
  },
  {
    id: "producto7",
    nombre: "Hamburguesa pig 2 quesos",
    precio: 35000,
    img: '<img style="width:70px;height:60px" src="img/H_pigdoblequeso.jpg" alt="">',
    stock: 3,
    carrito: 0,
  },
  {
    id: "producto8",
    nombre: "Hamburguesa Pig Costichips",
    precio: 37000,
    img: '<img style="width:70px;height:60px" src="img/H_pigcostichips.jpg" alt="">',
    stock: 3,
    carrito: 0,
  },
  {
    id: "producto9",
    nombre: "Hamburguesa chicharron caramelizado",
    precio: 34000,
    img: '<img style="width:70px;height:60px" src="img/H_chicharroncaramelizado.png" alt="">',
    stock: 3,
    carrito: 0,
  },
  {
    id: "producto10",
    nombre: "Hamburguesa chiken Pig",
    precio: 40000,
    img: '<img style="width:70px;height:60px" src="img/H_pigchiken.jpeg" alt="">',
    stock: 3,
    carrito: 0,
  },
  {
    id: "producto11",
    nombre: "Hamburguesa de res especial",
    precio: 35000,
    img: '<img style="width:70px;height:60px" src="img/h_res.png" alt="">',
    stock: 2,
    carrito: 0,
  },
  {
    id: "producto12",
    nombre: "Hamburguesa Res Doble Carne",
    precio: 40000,
    img: '<img style="width:70px;height:60px" src="img/h_doblerescarne.jpg" alt="">',
    stock: 2,
    carrito: 0,
  },
];

function validarLogin(){
  const currentLoggedUser = localStorage.getItem('loggedUser');
  if (!currentLoggedUser){
    window.location.href = 'login.html';
  }
}

function cargarLogin() {
  validarLogin();
  // let info = localStorage.getItem("loggedin");

  // let splitted_info = info.split(",");
  // let nombre = splitted_info[0];
  // let correo = splitted_info[1];

  // const element = document.getElementById("prof_div_1");
  // const element2 = element.querySelector("#prof_div_1_div_info");
  // const element3 = element2.querySelectorAll(":scope > span");

  // element3[0].textContent = nombre;
  // element3[1].textContent = correo;
  const currentLoggedUser = JSON.parse(localStorage.getItem('loggedUser'));
  
  let correoElement = document.getElementById("profile_email");
  let nombreElement = document.getElementById("profile_name");
  let telefonoElement = document.getElementById("profile_phone");
  let rolElement = document.getElementById("profile_role");

  correoElement.innerText = currentLoggedUser.correo;
  nombreElement.innerText = `${currentLoggedUser.nombre} ${currentLoggedUser.apellido1} ${currentLoggedUser.apellido2}`
  telefonoElement.innerText = currentLoggedUser.telefono;
  rolElement.innerText = currentLoggedUser["rol_usuario"]
}

cargarLogin();

function logOut() {
  location.href = "login.html";
  localStorage.removeItem('loggedUser');
}

function cargarStock() {
  for (let index = 0; index < products.length; index++) {
    let id = products[index].id;

    const element = document.getElementById(id);
    const element2 = element.querySelector(":scope > span");
    const element3 = element2.querySelector("#stock_unit");

    element3.textContent = " " + products[index].stock;
  }
}

cargarStock();

function actualizarStockHTMLIndividual(indice) {
  let id = products[indice].id;

  const element = document.getElementById(id);
  const element2 = element.querySelector(":scope > span");
  const element3 = element2.querySelector("#stock_unit");

  element3.textContent = " " + products[indice].stock;
}

function asignarAgotamiento(indice) {
  let id = products[indice].id;

  const element = document.getElementById(id);

  if (element.className != "box_out") {
    element.className = "box_out";
    element.insertAdjacentHTML(
      "beforeend",
      '<input type="button" class="btn_out" value="Agotado">'
    );
  }
}

function retirarAgotamiento(index) {
  let id = products[index].id;

  const element = document.getElementById(id);

  if (element.className != "box") {
    element.className = "box";
    let element2 = element.querySelectorAll(":scope > input");
    if ((element2 != null) & (element2 != undefined)) {
      element.removeChild(element2[2]);
    }
  }
}

function actualizarCarrito(index) {
  //Con indice hago referencia a un index tipo numérico
  // Y con indice me refiero a uno tipo string

  let id = products[index].id;
  let nombre = products[index].nombre;
  let imagen = products[index].img;
  let precio = products[index].precio;
  let carrito = products[index].carrito;

  // Esta variable se utiliza para generar nuevos div en el carrito
  // Pero también para identificar desde qué producto se clickea
  let element_id = "car" + id;

  let parentElement = document.getElementById("contenedor_carrito");
  let childrenList = parentElement.children;

  let for_boolean = false;
  for (let index = 0; index < childrenList.length; index++) {
    if (childrenList[index].id == element_id) {
      for_boolean = true;
      break;
    }
  }

  if (for_boolean == true) {
    const element = parentElement.querySelector("#" + element_id);
    const element2 = element.querySelector(":scope > div");
    const element3 = element2.querySelector(":scope > h3");
    const element4 = element3.querySelector("#item_cantidad");

    element4.textContent = products[index].carrito;
  } else {
    parentElement.insertAdjacentHTML(
      "afterbegin",
      '<div id="' +
        element_id +
        '" class="cart-item">' +
        '<span class="fas fa-times" onclick="retornarDeCarritoAStock(' +
        index +
        ')"></span>' +
        imagen +
        '<div class="content">' +
        "<h3>" +
        nombre +
        'X<span id="item_cantidad">' +
        carrito +
        "</span></h3>" +
        '<div class="price">' +
        precio +
        "</div>" +
        "</div>" +
        "</div>"
    );
  }
}

function añadirAlCarrito(indice) {
  let index = +indice;
  if (products[index].stock >= 1) {
    products[index].stock -= 1;
    actualizarStockHTMLIndividual(index);

    //Parte del carrito
    products[index].carrito += 1;
    actualizarCarrito(index);
  }

  if (products[index].stock == 0) {
    alert("Producto agotado");
    asignarAgotamiento(index);
  }

  //cartItem.classList.toggle('active');
}

function retornarDeCarritoAStock(index) {
  products[index].stock += products[index].carrito;
  products[index].carrito = 0;

  let parentElement = document.getElementById("contenedor_carrito");
  let element = parentElement.querySelector("#car" + products[index].id);
  parentElement.removeChild(element);

  actualizarStockHTMLIndividual(index);
  retirarAgotamiento(index);
}

let navbar = document.querySelector(".navbar");

document.querySelector("#menu-btn").onclick = () => {
  navbar.classList.toggle("active");
  searchForm.classList.remove("active");
  cartItem.classList.remove("active");
};

let searchForm = document.querySelector(".search-form");

document.querySelector("#search-btn").onclick = () => {
  searchForm.classList.toggle("active");
  /* navbar.classList.remove('active'); */
  cartItem.classList.remove("active");
};

let cartItem = document.querySelector(".cart-items-container");

document.querySelector("#cart-btn").onclick = () => {
  cartItem.classList.toggle("active");
  /* navbar.classList.remove('active'); */
  searchForm.classList.remove("active");
};

window.onscroll = () => {
  // navbar.classList.remove('active');
  // searchForm.classList.remove('active');
  // cartItem.classList.remove('active');
};
