from django.shortcuts import render
from django.contrib.auth.decorators import login_required
from .models import Producto


# Create your views here.
@login_required
def store(request):
    productos = Producto.objects.all()
    return render(request, 'store.html', {'productos': productos})


# Create your views here.
@login_required
def product(request, productId):
    products = Producto.objects.filter(id=productId)
    if products.exists():
        product = products.first()
        return render(request, 'product.html', {'producto': product})
    print(product)
    # productos = Producto.objects.all()
