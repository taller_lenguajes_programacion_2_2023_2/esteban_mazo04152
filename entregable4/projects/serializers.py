from rest_framework import serializers
from .models import Persona, Usuario

class PersonaSerializer(serializers.ModelSerializer):
     class Meta:
          model = Persona
         # fields = ('cedula', 'nombre', 'apellido1', 'apellido2', 'telefono')
          fields = '__all__'
          
class UsuarioSerializer(serializers.ModelSerializer):
        persona = PersonaSerializer(read_only = True)
        class Meta:
             model = Usuario
             #fields = ('correo', 'password', 'conf_pass', 'rol_usuario', 'fecha_cre')
             fields = '__all__'
             read_only_fields = ('fecha_cre', )
    
""" 
class UsuarioPersonaSerializer(serializers.ModelSerializer):
    class Meta:
        model =Usuario
        fields = '__all__'
        read_only_fields = ('fecha_cre', )

    def create(self, validated_data):
         persona_data = validated_data.pop('persona', None)
         usuario = Usuario.objects.create (*validated_data)

         if persona_data:
             Persona.objects.create(usuario=usuario, *persona_data)
         return Usuario
"""