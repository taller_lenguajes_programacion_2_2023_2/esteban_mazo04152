from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from django.contrib.auth.models import BaseUserManager
from django.db import models
import random



# Create your models here.
class Persona(models.Model):
    cedula = models.IntegerField(unique=True)
    nombre = models.CharField(max_length=100)
    apellido1 = models.CharField(max_length=100)
    apellido2 = models.CharField(max_length=100)
    telefono = models.IntegerField()
    last_login = models.DateTimeField(null=True, blank=True)

    def __str__(self):
        return f"{self.cedula} {self.nombre} {self.apellido1} {self.apellido2} {self.telefono}"


class UsuarioManager(BaseUserManager):
    def create_user(self, correo, password=None, **kwargs):
        if not correo:
            raise ValueError('El usuario debe tener un correo electrónico')
        usuario = self.model(correo=correo, conf_pass=password, is_staff=False, is_superuser=False,
                             cedula=random.randint(2334, 2323233223), nombre='admin', apellido1='admin', apellido2='admin', telefono=random.randint(2334, 2323233223))
        usuario.set_password(password)
        usuario.save()
        return usuario

    def create_superuser(self, correo, password, **kwargs):
        usuario = self.create_user(correo, password=password, is_staff=True, is_superuser=True, **kwargs)
        usuario.is_staff = True
        usuario.is_superuser = True
        usuario.save()
        return usuario


class Usuario(Persona, AbstractBaseUser, PermissionsMixin):
    correo = models.CharField(max_length=100, unique=True)
    password = models.CharField(max_length=100)
    conf_pass = models.CharField(max_length=100)
    fecha_cre = models.DateTimeField(auto_now_add=True)
    is_staff = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)

    USERNAME_FIELD = 'correo'
    REQUIRED_FIELDS = ['password', 'conf_pass']

    objects = UsuarioManager()

    def __str__(self):
        return f"Usuario: {self.correo} {self.password} {self.conf_pass} - {super().__str__()}"
